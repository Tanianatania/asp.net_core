﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using asp.net_core.Services;
using asp.net_core.Interfaces;


namespace asp.net_core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {  
            Configuration = configuration;
            Parking park=new Parking(); 
            //Program.GetRequesr("");
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSingleton<IParkingService,ParkingsService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();

            /* app.Run(async (next) =>
            {
               //Program.Run();
            });

            app.Use(async (context, next) =>
        {
            Console.WriteLine("Started handling request");
            await next.Invoke();
            Console.WriteLine("Finished handling request");
        });*/
            app.UseMvc();
        }
    }
}
