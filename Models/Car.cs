using Newtonsoft.Json;
public class Car
{
    [JsonProperty("name")]
    public string Name { get; set; }
    [JsonProperty("carnumber")]
    public string CarNumber { get; set; }
    [JsonProperty("balance")]
    public decimal Balance { get; set; }

    public Car(string name, string carnumber,decimal balance)
    {
        Name=name;
        CarNumber=carnumber;
        Balance=balance;
    }
}