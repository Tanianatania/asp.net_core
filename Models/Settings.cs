public static class Setting
    {
        private static readonly decimal balance = 0;
        private static readonly int countOfPlace =10;
        private static readonly int pediodOfTime = 5;
        public static readonly decimal carRate = 2;
        public static readonly decimal trakRate = 5;
        public static readonly decimal busRate = 3.5M;
        public static readonly decimal motorcycleRate = 1;
        public static readonly decimal fine = 2.5M;
        public static string filePath = @"Transactions.log";

        public static decimal GetBalance()
        {
            return balance;
        }

        public static int GetCountOfAvaliblePlace()
        {
            return countOfPlace;
        }

        public static int GetCountOfPlace()
        {
            return countOfPlace;
        }
        public static int GetPeriodOfTime()
        {
            return pediodOfTime;
        }
    }