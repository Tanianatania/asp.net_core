using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Timers;
using Newtonsoft.Json;
public class Parking
{
    [JsonProperty("balance")]
    public decimal Balance = Setting.GetBalance();
    [JsonProperty("countofplace")]
    private int countOfPlace = Setting.GetCountOfAvaliblePlace();
    [JsonProperty("transactions")]
    public static List<Transaction> Transactions = new List<Transaction>();
    [JsonProperty("cars")]
    public static List<Car> Cars = new List<Car>();
    [JsonProperty("fine")]
    private decimal Fine;

    public decimal GetTotalBalance()
    {
        return Balance;
    }

    public List<Car> GetListOfCar()
    {
        return Cars;
    }

    public List<Transaction> GetListOfTransaction()
    {
        return Transactions;
    }

    public int CountOfFreePlace()
    {
        return countOfPlace - Cars.Count;
    }

    public static bool IsCarNumberFree(string _name)
    {
        foreach (var item in Parking.Cars)
        {
            if (item.CarNumber == _name)
            {
                return false;
            }
        }
        return true;
    }  

    
}