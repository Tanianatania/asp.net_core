using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
public class Transaction
{
    [JsonProperty("time")]
    public DateTime Time { get; set; }
    [JsonProperty("cartype")]
    public string CarType { get; set; }
    [JsonProperty("carnumber")]
    public string CarNumber { get; set; }
    [JsonProperty("salary")]
    public decimal Salary { get; set; }

    public Transaction(string _carType, string _carNumber, decimal _salary, DateTime _time)
    {
        CarType = _carType;
        CarNumber = _carNumber;
        Salary = _salary;
        Time = _time;
    }

    public void Dasplay()
    {
        Console.WriteLine(CarType, CarNumber, Salary.ToString(), Time.ToString());
    }
}