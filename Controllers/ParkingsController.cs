using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asp.net_core.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.IO;
using System.Timers;
using asp.net_core.Interfaces;

namespace asp.net_core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingsController : ControllerBase
    {
        private IParkingService parkingrsService;

        public ParkingsController(IParkingService parking)
        {
            parkingrsService = parking;
        }

        // GET api/parkings/getbalance
        [Route("getbalance")]
        [HttpGet]
        public ActionResult<decimal> GetBalance()
        {
            return parkingrsService.GetBalance();
        }

        // GET api/parkings/listofcar
        [Route("listofcar")]
        [HttpGet]
        public ActionResult<List<Car>> GetListOfCar()
        {
            return parkingrsService.GetListOfCar();
        }

        [Route("displayalltransaction")]
        [HttpGet]
        public ActionResult<List<Transaction>> DisplayTransactions()
        {
            return parkingrsService.DisplayTransactions();
        }

        [Route("addcar/{name}/{carnumber}/{balance}")]
        [HttpGet]
        public void AddCar(string name, string carnumber, string balance)
        {
            decimal _balance = Convert.ToDecimal(balance);
            Car car = new Car(name, carnumber, _balance);
            parkingrsService.CreateProductAsync(car);
        }

        [Route("addmoney/{_carNumber}/{money}")]
        [HttpPut]
        public void AddMoneyToCar(string _carNumber, string money)
        {
            parkingrsService.AddMoneyToCar(_carNumber, money);
        }

        [Route("addtransaction")]
        [HttpGet]
        public void AddTransaction()
        {
            parkingrsService.AddTransaction();
        }

        [Route("writetofile")]
        [HttpGet]
        public void WriteToFile()
        {
            parkingrsService.WriteToFile();
        }

        
        [Route("deletecar/{carnumber}")]
        [HttpGet]
        public void DeleteCar(string carnumber)
        {
            parkingrsService.DeleteCar(carnumber);
        }

        // GET api/parkings/getcount
        [Route("getcount")]
        [HttpGet]
        public ActionResult<int> GetCountOfAvaliblePvace()
        {
            return parkingrsService.GetCountOfAvaliblePlace();
        }
    }
}