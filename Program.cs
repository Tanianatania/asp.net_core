﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Timers;

namespace asp.net_core
{
    public class Program
    {
        public static bool Exit()
        {
            bool result;
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(" Do you want continue?\n" +
                "\t1 - Yes\n" +
                "\t2 - No");
            Console.ResetColor();
            string value = Console.ReadLine();
            int key = Int32.Parse(value);
            if (key == 1)
            {
                return false;
            }
            else if (key == 2)
            {
                return true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The number should be in the range of 1 to 2");
                Console.ResetColor();
                result = Exit();
                return result;
            }
        }
        public static string GetCarNumber()
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Enter car number:");
            Console.ResetColor();
            string carNumber = Console.ReadLine();
            return carNumber;
        }     

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public async static void GetRequesr(string str)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:5001/");
                using (HttpResponseMessage responce 
                = await client.GetAsync(str))
                {
                    using (HttpContent content = responce.Content)
                    {
                        string myContent = await content.ReadAsStringAsync();
                        Console.WriteLine(myContent);
                    }
                }
            }
        }

        public static void Run()
        {
            int perid = Setting.GetPeriodOfTime();
            Timer timer = new System.Timers.Timer(perid * 1000);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(AddTransaction);
            timer.Start();

            Timer fileTimer = new System.Timers.Timer(60000);
            fileTimer.AutoReset = true;
            timer.Enabled = true;
            fileTimer.Elapsed += new System.Timers.ElapsedEventHandler(WriteToFile);
            fileTimer.Start();

            Parking park=new Parking();
            bool exit = false;
            int key = 0;

            string carNumber = "";
            Console.ResetColor();
            Console.WriteLine(" Hello, welcome to out Parking Park))");
            try
            {
                while (exit != true)
                {
                    Console.WriteLine(" Please choose from the list what you would like to do:");
                    Console.WriteLine("\t1 - find out the current balance of Parking Park\n" +
                        "\t2 - Find out the amount of money earned in the last minute\n" +
                        "\t3 - Find out the count of free place in Parking Park\n" +
                        "\t4 - Show all Parking Park transactions in the last minute\n" +
                        "\t5 - Print the entire transaction history\n" +
                        "\t6 - Display the list of all Vehicles\n" +
                        "\t7 - Put a Vehicle on Parking\n" +
                        "\t8 - Take away the Parking Truck\n" +
                        "\t9 - Refill the balance of the Vehicle\n" +
                        "\t10 - Exit");


                    string value = Console.ReadLine();
                    key = Int32.Parse(value);

                    Console.ForegroundColor = ConsoleColor.Green;
                    switch (key)
                    {
                        case 1:
                           GetRequesr("parkings/getbalance");               
                            exit = false;
                            break;
                        case 2:
                            
                            exit = false;
                            break;
                        case 3:
                            GetRequesr("parkings/getcount");
                            exit = false;
                            break;
                        case 4:
                             GetRequesr("parkings/listofcar");
                            exit = false;
                            break;
                        case 5:
                            //park.DisplayAllTransactions();
                            exit = false;
                            break;
                        case 6:
                            GetRequesr("parkings/displayalltransaction");
                            exit = false;
                            break;
                        case 7:
                            carNumber=GetCarNumber();
                            string request="parkings/getcount/car/"+carNumber+"/0";
                            GetRequesr(request);
                            exit = false;
                            break;
                        case 8:
                            carNumber = GetCarNumber();
                            request="parkings/deletecar/"+carNumber;
                            GetRequesr(request);
                            exit = false;
                            break;
                        case 9:
                            carNumber = GetCarNumber();
                            if (carNumber == "")
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Car can not have null car number");
                                Console.ResetColor();
                                exit = Exit();
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.WriteLine("Enter count of money:");
                            Console.ResetColor();
                            exit = false;
                            break;
                        case 10:
                            exit = true;
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("The number should be in the range of 1 to 10");
                            Console.ResetColor();
                            exit = Exit();
                            break;
                    }
                     Console.ResetColor();
                }
            }
            catch (FormatException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The input data should be number");
            }
            catch (OverflowException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Value is too large or too small");
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);
            }
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("GoodBye!!)))");
            Console.ResetColor();
            string name = Console.ReadLine();          
        }

        public static void AddTransaction(object sender, ElapsedEventArgs e)
        {
            GetRequesr("parkings/addtransaction");
        }

        public static void WriteToFile(object sender, ElapsedEventArgs e)
        {
            GetRequesr("parkings/writetofile");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
