using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Text;
using System.IO;
using System.Timers;
using asp.net_core.Interfaces;

namespace asp.net_core.Services
{
    public class ParkingsService:IParkingService
    {
        private HttpClient _client;
        private static Parking park = new Parking();

        public ParkingsService()
        {
            _client = new HttpClient();
        }
        public void CreateProductAsync(Car car)
        {
            List<Car> Cars = park.GetListOfCar();
            int countOfPlace = park.CountOfFreePlace();

            if (Cars.Count < countOfPlace)
            {
                if (Parking.IsCarNumberFree(car.CarNumber))
                {
                    Cars.Add(car);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Car was added");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("This car number is exist");
                    Console.ResetColor();
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("We don`t have free plase. Sorry(((((((");
                Console.ResetColor();
            }
        }

        public void DeleteCar(string _carNumber)
        {
            Car _car;
            List<Car> Cars = park.GetListOfCar();
            foreach (var car in Cars)
            {
                if (car.CarNumber == _carNumber)
                {
                    _car = car;
                    Cars.Remove(_car);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Car was remove");
                    Console.ResetColor();
                    return;
                }
            }
        }

        public List<Car> GetListOfCar()
        {
            var result = JsonConvert.SerializeObject(park.GetListOfCar());
            return JsonConvert.DeserializeObject<List<Car>>(result);
        }
        
        public decimal GetBalance()
        {
            var result = park.GetTotalBalance().ToString();
            return JsonConvert.DeserializeObject<decimal>(result);
        }

        public Car FindCarByCarNumber(string _carNumber)
        {
            Car car = null;
            foreach (var item in Parking.Cars)
            {
                if (item.CarNumber == _carNumber)
                {
                    return item;
                }
            }
            return car;
        }

        public void AddMoneyToCar(string _carNumber, string _money)
        {
            decimal money = Convert.ToDecimal(_money);
            Car _car = FindCarByCarNumber(_carNumber);
            _car.Balance += money;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Balance was update");
            Console.ResetColor();
        }

        public decimal GetRate(string _name)
        {
            switch (_name)
            {
                case "car":
                    return Setting.carRate;
                case "truck":
                    return Setting.trakRate;
                case "bus":
                    return Setting.busRate;
                case "motorbyke":
                    return Setting.motorcycleRate;
                default:
                    return 0;
            }
        }

        public List<Transaction> DisplayTransactions()
        {               
            var result = JsonConvert.SerializeObject(park.GetListOfTransaction());
            return JsonConvert.DeserializeObject<List<Transaction>>(result);
        }
        
        public void AddTransaction()
        {
            List<Car> Cars = park.GetListOfCar();
            foreach (var item in Cars)
            {
                decimal _salary = GetRate(item.Name);
                if (item.Balance <= _salary)
                {
                    _salary = item.Balance * Setting.fine;
                }
                else
                {
                    _salary = item.Balance;
                }

                Transaction _transaction = new Transaction(item.Name, item.CarNumber, _salary, DateTime.Now);
                Parking.Transactions.Add(_transaction);
                park.Balance += _salary;
            }
        }
        public void WriteToFile()
        {
            using (StreamWriter file = new StreamWriter(Setting.filePath))
            {
                foreach (var item in Parking.Transactions)
                {
                    string elem = item.CarType + " " + item.CarNumber + " " + item.Salary + " " + item.Time;
                    file.WriteLine(elem);
                }
                Parking.Transactions = new List<Transaction>();
            }
        }

        public int GetCountOfAvaliblePlace()
        {
            Parking park = new Parking();
            var result = park.CountOfFreePlace().ToString();
            return JsonConvert.DeserializeObject<int>(result);
        }
    }
}