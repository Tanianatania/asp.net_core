using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asp.net_core.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.IO;
using System.Timers;

namespace asp.net_core.Interfaces
{
    public interface IParkingService
    {
        void DeleteCar(string _carNumber);

        void CreateProductAsync(Car car);
        List<Car> GetListOfCar();
        decimal GetBalance();

        Car FindCarByCarNumber(string _carNumber);

        void AddMoneyToCar(string _carNumber, string _money);

        decimal GetRate(string _name);

        List<Transaction> DisplayTransactions();
        
        void AddTransaction();
        void WriteToFile();

        int GetCountOfAvaliblePlace();
    }
}
